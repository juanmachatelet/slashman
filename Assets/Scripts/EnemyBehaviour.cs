﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour
{

    public int hp;
    public Text hpText;
    // Start is called before the first frame update
    void Start()
    {
        hpText.text = hp.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(hp <= 0){
            hpText.text = "DEAD";
        }
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Fireball")){
            hp -= 25;
            hpText.text = hp.ToString();
            collision.gameObject.SendMessage("DestroyGameObject");
        }
    }

    private void OnTriggerEnter2D(Collider2D collider){
        if(collider.tag == "Fireball"){
            hp -= 25;
            hpText.text = hp.ToString();
            collider.gameObject.SendMessage("DestroyGameObject");
        }
    }
}
