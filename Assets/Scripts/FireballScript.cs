﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour
{
    public GameObject fireball;
    private bool canFire = true;
    public GameObject fireButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void throwFireball(){
        if(canFire){
            GameObject tempFireball = Instantiate(fireball, new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z), Quaternion.identity);
            tempFireball.transform.Rotate(new Vector3(0,0,270));
            tempFireball.GetComponent<Animator>().SetTrigger("throwFb");
            tempFireball.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 8);
            canFire = false;
            StartCoroutine(FireballCooldown());
        }
    }
    IEnumerator FireballCooldown(){
        fireButton.SetActive(false);
        yield return new WaitForSeconds(3);
        fireButton.SetActive(true);
        canFire = true;
    }
}
