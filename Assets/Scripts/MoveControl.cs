﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveControl : MonoBehaviour
{

    public GameObject circle;
    public GameObject dot;
    private Rigidbody2D rb;
    public float moveSpeed;
    private Touch oneTouch;
    private Vector2 touchPosition;
    private Vector2 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        circle.SetActive(false);
        dot.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0){
            oneTouch = Input.GetTouch(0);
            touchPosition = Camera.main.ScreenToWorldPoint(oneTouch.position);

            switch (oneTouch.phase)
            {
                case TouchPhase.Began:
                    circle.SetActive(true);
                    dot.SetActive(true);

                    circle.transform.position = touchPosition;
                    dot.transform.position = touchPosition;
                    break;
                case TouchPhase.Stationary:
                    MovePlayer();
                    break;
                case TouchPhase.Moved:
                    MovePlayer();
                    break;
                case TouchPhase.Ended:
                    circle.SetActive(false);
                    dot.SetActive(false);
                    rb.velocity = Vector2.zero;
                    break; 
            }
        }
    }

    private void MovePlayer(){
        dot.transform.position = touchPosition;
        dot.transform.position = new Vector2(
            Mathf.Clamp(dot.transform.position.x,
            circle.transform.position.x - 0.44f,
            circle.transform.position.x + 0.44f),
            Mathf.Clamp(dot.transform.position.y,
            circle.transform.position.y - 0.44f,
            circle.transform.position.y + 0.44f));

        moveDirection = (dot.transform.position - circle.transform.position).normalized;
        rb.velocity = moveDirection * moveSpeed;
    }

    // public static float Angle(Vector2 p_vector2)
    //  {
    //      if (p_vector2.x < 0)
    //      {
    //          return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
    //      }
    //      else
    //      {
    //          return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;
    //      }
    //  }
}
